Readme

This project aims at creating a machine learning model using perceptron to correct homonyms. Brown corpus was used as training data. Accuracy on test data was 93%.

Approach:

I have used two previous words and two next words along with their POS tags as the feature vector for the classes.
I have used brown corpus formatted and merged into single file with no pos tags attached as training data.

How to run the code:

** Filename: create_train.py 
Execution: python create_train.py <Training_File> <Development_File>
I have used brown corpus from nltk as my training file and hw3.dev.txt as my development file.
This program calls perceplearn.py and perceplearn.py generates the model file.
create_train.py calls perceplearn 5 time for each set of classes, ex: "its,it's","they're,their" etc
The output of this program are 5 model files named 1.model, 2.model etc
Also it generates training and development files as intermediate file for each set of class named 1.train, 1.dev etc.

** Filename: classifier.py
Execution: python classifier.py <TestFile> <TestFile_corrected>
This script reads the 5 models and corrects the test file for each set of classes.

** Filename acc_c.py
Execution: python acc_c.py <output_file> <correct_file>
It will print accuracy for the output file.
I have used python's nltk library to POS tag the training and test file.

The training data used is from the brown corpus available with nltk.

nltk.pos_tag() is used for POS tagging.