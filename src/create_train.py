import sys,re,string,nltk
from subprocess import call

f = open(sys.argv[1],'r')
d = open(sys.argv[2],'r')
regex = re.compile('[%s]' % re.escape(string.punctuation))

pos_tag = []
for line in f.read().splitlines():
        words = line.split()
        pos_tagged = nltk.pos_tag(words)
        pos_tag.append(pos_tagged)

for j in range(len(pos_tag)):
        pos_tag[j].insert(0,("BOS","BOS/POS"))
        pos_tag[j].insert(1,("next_BOS","next_BOS/POS"))
        pos_tag[j].append(("prev_EOS","prev_EOS/POS"))
        pos_tag[j].append(("EOS","EOS/POS"))        

pos_tag_dev = []
for line in d.read().splitlines():
        words = line.split()
        pos_tagged = nltk.pos_tag(words)
        pos_tag_dev.append(pos_tagged)


for j in range(len(pos_tag_dev)):
        pos_tag_dev[j].insert(0,("BOS","BOS/POS"))
        pos_tag_dev[j].insert(1,("next_BOS","next_BOS/POS"))
        pos_tag_dev[j].append(("prev_EOS","prev_EOS/POS"))
        pos_tag_dev[j].append(("EOS","EOS/POS"))        
        
total_class = [["it's","its"],["you're","your"],["they're","their"],["loose","lose"],["to","too"]]


for i in range(len(total_class)):
    print "Creating model for "+'"'+total_class[i][0]+" "+total_class[i][1]+'"'
    p=open(str(i)+'.train','w')
    p.truncate()
    p=open(str(i)+'.train','a')
    
    classes = total_class[i]

    ###create training file
    for j in range(len(pos_tag)):
        for k in range(len(pos_tag[j])):
            for clas in classes:
                if clas == pos_tag[j][k][0] or string.capwords(clas) == pos_tag[j][k][0]:
                    line_f = clas+" prev_"+pos_tag[j][k-1][0]+" pr_po"+pos_tag[j][k-1][1]+" prev_prev_"+pos_tag[j][k-2][0]+" pr_pr_po"+pos_tag[j][k-2][1]+" next"+pos_tag[j][k+1][0]+" ne_po"+pos_tag[j][k+1][1]+" next_next_"+pos_tag[j][k+2][0]+" ne_ne_po"+pos_tag[j][k+2][1]+"\n"
                    p.write(line_f)    
    p.close()
    
    ###create development file
                    
    de=open(str(i)+'.dev','w')
    de.truncate()
    de=open(str(i)+'.dev','a')            
    for j in range(len(pos_tag_dev)):
        for k in range(len(pos_tag_dev[j])):
            for clas in classes:
                if clas == pos_tag_dev[j][k][0] or string.capwords(clas) == pos_tag_dev[j][k][0]:
                    line_f = clas+" prev_"+pos_tag_dev[j][k-1][0]+" pr_po"+pos_tag_dev[j][k-1][1]+" prev_prev_"+pos_tag_dev[j][k-2][0]+" pr_pr_po"+pos_tag_dev[j][k-2][1]+" next"+pos_tag_dev[j][k+1][0]+" ne_po"+pos_tag_dev[j][k+1][1]+" next_next_"+pos_tag_dev[j][k+2][0]+" ne_ne_po"+pos_tag_dev[j][k+2][1]+"\n"
                    de.write(line_f)    
    de.close()
    
    
    call(["python3","perceplearn.py",str(i)+".train",str(i)+".model","-h",str(i)+".dev"])
          
    
