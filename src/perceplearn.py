from collections import Counter
from random import shuffle

import math,sys,time,copy, argparse
starttime = time.time()

def main():
    parser = argparse.ArgumentParser(conflict_handler='resolve')
    parser.add_argument("trainingfile",help="dev file name")
    parser.add_argument("modelfile",help="dev file name")
    parser.add_argument("-h","--devfile",help="dev file name")
    args = parser.parse_args()

    p=open(args.modelfile,'w')
    p.truncate()
    p=open(args.modelfile,'a')
    time
    f=open(args.trainingfile,'r')
    
    lines = f.read().splitlines()

    classes = []
    for i in lines:
        firstword = i.split()[0]
        if firstword not in classes:
            classes.append(firstword)
    print (classes)        

    weight_vec = [{} for _ in range(len(classes))]
    weight_avg = [{} for _ in range(len(classes))]
    vocab = set()

    for line in lines:
        for word in line.split()[1:]:
            vocab.add(word)
            for classvec in weight_vec:
                #if word not in classvec:
                classvec[word] = float(0)
            for classvec in weight_avg:
                #if word not in classvec:
                classvec[word] = float(0)

    max_accuracy = 0            

    for n in range(25):
        print(time.time()-starttime)
        shuffle(lines)
        for line in lines:
            #print(line)
            words = line.split()
            y = words[0]
            for i in range(len(classes)):
                if y == classes[i]:
                    correctclass = i

            argmax = [0]*len(classes)
            for i in range(len(classes)):
                for word in words[1:]:
                    argmax[i] = argmax[i]+weight_vec[i][word]
            #print (argmax)

            index = argmax.index(max(argmax))
            #print(max(argmax))
            #print(index)    
            if classes[index] != y:
                #print("inside index"+classes[index]+" "+y)
                for word in words[1:]:
                    weight_vec[index][word] = weight_vec[index][word] - 1
                    weight_vec[correctclass][word] = weight_vec[correctclass][word] + 1


        for i in range(len(classes)):
            for word in vocab:
                weight_avg[i][word] = weight_avg[i][word]+weight_vec[i][word]
                #weight_avg[correctclass][word] = weight_avg[correctclass][word]+weight_vec[correctclass][word]

    ######Dev set testing part
        #print (args.devfile)
        if args.devfile:
            devfile = open(args.devfile,'r')
            devlines = devfile.read().splitlines()
            count = 0
            correct = 0
            for line in devlines:
                count=count+1
                prob = [0]*len(classes)
                for i in range(len(classes)):
                    words = line.split()
                    for word in words[1:]:
                        if word in weight_avg[i]:
                            prob[i] = prob[i]+weight_avg[i][word]
                index = prob.index(max(prob))
                #f2.write(classes[index]+"\n")
                if(classes[index]==words[0]):
                    correct=correct+1
            print("accuracy "+str(correct/count))
            if ((correct/count)>max_accuracy):
                max_accuracy = correct/count
                weight_optimal = copy.deepcopy(weight_avg)

    ##########Dev set testing part

    for i in range(len(classes)):
        p.write(classes[i]+" ")
        for word in vocab:
            p.write(word+":break:"+str(weight_optimal[i][word])+" ")
        p.write("\n")    

if __name__ == '__main__':
    main()
                     
