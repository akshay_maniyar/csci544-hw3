import sys,re,string,nltk
from subprocess import call

f = open(sys.argv[1],'r')
regex = re.compile('[%s]' % re.escape(string.punctuation))

pos_tag = []
for line in f.read().splitlines():
        words = line.split()
        pos_tagged = nltk.pos_tag(words)
        pos_tag.append(pos_tagged)

for j in range(len(pos_tag)):
        pos_tag[j].insert(0,("BOS","BOS/POS"))
        pos_tag[j].insert(1,("next_BOS","next_BOS/POS"))
        pos_tag[j].append(("prev_EOS","prev_EOS/POS"))
        pos_tag[j].append(("EOS","EOS/POS"))        

for p in range(5):
    
    f = open(str(p)+".model",'r')
    lines_m = f.read().splitlines()
    f.close()
    classes = []
    for line in lines_m:
        firstword = line.split()[0]
        if firstword not in classes:
            classes.append(firstword)

    weight_vec = [{} for _ in range(len(classes))]

    for i in range(len(classes)):
        words = lines_m[i].split()
        for word in words[1:]:
            keyval = word.split(':break:')
            weight_vec[i][keyval[0]] = float(keyval[1])
            
    count = 0
    correct = 0
    for j in range(len(pos_tag)):
        for k in range(len(pos_tag[j])):
            for clas in classes:
                if clas == pos_tag[j][k][0] or string.capwords(clas) == pos_tag[j][k][0]:
                    #line_f = clas+" prev_"+pos_tag[j][k-1][0]+" pr_po"+pos_tag[j][k-1][1]+" prev_prev_"+pos_tag[j][k-2][0]+" pr_pr_po"+pos_tag[j][k-2][1]+" next"+str(pos_tag[j][k+1][0])
                    #print line_f
                    #print pos_tag[j][k+1][0]
                    original = pos_tag[j][k][0]    
                    line_f = clas+" prev_"+pos_tag[j][k-1][0]+" pr_po"+pos_tag[j][k-1][1]+" prev_prev_"+pos_tag[j][k-2][0]+" pr_pr_po"+pos_tag[j][k-2][1]+" next"+str(pos_tag[j][k+1][0])+" ne_po"+pos_tag[j][k+1][1]+" next_next_"+pos_tag[j][k+2][0]+" ne_ne_po"+pos_tag[j][k+2][1]+"\n"
                    #print line_f
                    #sys.stdout.write(line_f)

                    prob = [0]*len(classes)
                    for l in range(len(classes)):
                        words_formatted = line_f.split()
                        for word in words_formatted[1:]:
                            if word in weight_vec[l]:
                                prob[l] = prob[l]+weight_vec[l][word]

                    index = prob.index(max(prob))
                    classified = classes[index]
                    if original[0].isupper():
                            classified = string.capwords(classified)
                    #print classified
                    #print pos_tag[j][k]
                    pos_tag[j][k] = nltk.pos_tag([classified])[0]
                    #pos_tag[j][k][1] = nltk.pos_tag([pos_tag[j][k][0]])[1]
                #print line
        
            
for j in range(len(pos_tag)):
        #print pos_tag[j]
        for k in range(2,len(pos_tag[j])-2):
                sys.stdout.write(pos_tag[j][k][0]+" ")
        sys.stdout.write("\n")
        
                
                               
